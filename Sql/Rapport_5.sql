SELECT t.train_id N_TRAIN, a.city ||'(' || TO_CHAR(t.departure_time, 'DD/MM/YYYY HH24:MI') || ')' || '-' || b.city || '(' || TO_CHAR(t.arrival_time, 'DD/MM/YYYY HH24:MI') || ')' VILLE_ET_DATE, t.distance DISTANCE, t.price PRIX
FROM T_TRAIN t
JOIN T_STATION a
ON t.departure_station_id = a.station_id
JOIN T_STATION b
ON t.arrival_station_id = b.station_id
ORDER BY t.train_id;
