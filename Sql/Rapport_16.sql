SELECT e.salary+100 "SALAIRE+100", e.employee_id N_EMPLOYEE, UPPER(e.last_name) || ' ' || e.first_name NOM_ET_PRENOM
FROM T_EMPLOYEE e
WHERE e.employee_id = ANY
(SELECT DISTINCT emp.employee_id
FROM T_EMPLOYEE emp
JOIN T_RESERVATION r
ON r.employee_id = emp.employee_id);
