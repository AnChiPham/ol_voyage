SELECT p.pass_name NOM_ABONNEMENT
FROM T_PASS p
JOIN T_CUSTOMER c
ON p.pass_id = c.pass_id
GROUP BY c.pass_id, p.pass_name
ORDER BY COUNT(c.pass_id)desc;
