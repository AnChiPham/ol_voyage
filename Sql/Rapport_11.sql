SELECT train_id || ' ' || d.city || ' – ' || a.city NOM_DU_TRAIN, pass_name TITRE,(t_train.price*discount_pct)/100 PRIX_REDUIT, (t_train.price*discount_we_pct)/100 PRIX_REDUIT_WEEK_END
FROM T_TRAIN
CROSS JOIN T_PASS JOIN T_STATION d
ON d.station_id = departure_station_id
JOIN T_STATION a
ON a.station_id = arrival_station_id AND d.city = 'Paris' ORDER BY train_id;
